first = 10
second = 30

result_sum = first + second
result_multiplication = first * second
result_division = second / first
result_subtraction = first - second
result_degree = first ** second
result_private_integer = second // first
result_remainder_division = second % first
result_sign_change = - first
result_Identity = + result_sign_change

print(result_sum)
print(result_multiplication)
print(result_division)
print(result_subtraction)
print(result_degree)
print(result_private_integer)
print(result_remainder_division)
print(result_sign_change)
print(result_Identity)

# От курса ожидаю полного погружения в новую специализацию.
# Как минимум научиться писать чат бота под ТГ, как максимум освоить одно из напрвлений Data Saints.
# Полученные знаня хочу попробовать совместить с существующей специализацией.
# Так же рассматриваю вариант полной смены рода деятельности.
# Жертвовать готов большим количеством времени, упорством ну и финансы соответственно.))